changeThemeButton = document.getElementById("changeTheme");
changeThemeButton.addEventListener("click", changeTheme);
if (localStorage.darkTheme == "true") {
    document.body.classList.add("dark");
}

function changeTheme() {
    if (document.body.classList.contains("dark")) {
        localStorage.setItem("darkTheme", "false");
        document.body.classList.remove("dark");
    } else {
        localStorage.setItem("darkTheme", "true");
        document.body.classList.add("dark");
    }
}

